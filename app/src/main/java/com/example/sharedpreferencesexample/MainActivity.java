package com.example.sharedpreferencesexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    final String TAG="JENELLE";

    //1. Create a shared preferences variables
    SharedPreferences prefs;

    //1a. Give your local storage a unique name
    public static final String PREFERENCES_NAME = "JenelleSP";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 2. Initialize and configure your shared preferences variable
        prefs = getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);

    }

    public void saveButtonPressed(View view) {
        Log.d(TAG, "Save button pressed!");

        // 3. save person's name to local storage (SharedPreferences)

        // 3a. open your shared preferences so you can add things to it
        SharedPreferences.Editor prefsEditor = prefs.edit();

        // 3b. save something to the file

        // - get the name from the text box
        EditText e = (EditText) findViewById(R.id.editText);
        String name = e.getText().toString();

        // - save it to shared preferences
        // { "username": name }
        prefsEditor.putString("username", name);


        // 3c. COMMIT your changes
        prefsEditor.apply();

        // 4. show success message
        Toast t = Toast.makeText(getApplicationContext(), "Data Saved", Toast.LENGTH_SHORT);
        t.show();


    }

    public void getDataButtonPressed(View view) {
        Log.d(TAG, "Get Data button pressed!");

        // get person's name from local storage (SharedPreferences)

        // parameter 1 = the name of the KEY you put in local storage
        // parameter 2 = if KEY does not exist, then what value do you want
        //      String n to have?
        String n = prefs.getString("username", "");

        // Output it to the screen!
        TextView resultLabel = (TextView) findViewById(R.id.resultsTextView);
        resultLabel.setText("Your name is: " + n);

    }



















}
